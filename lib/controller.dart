import 'package:flutter/cupertino.dart';
import 'package:flutter_image_upload/service.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';

class ProfileController extends GetxController {
  var isLoading = false.obs;
  var imageURL = '';

  void uploadImage(ImageSource imageSource) async {
    try {
      final pickedFile = await ImagePicker().getImage(source: imageSource);
      isLoading(true);
      if (pickedFile != null) {
        var response = await ImageService.uploadFile(pickedFile.path);
        print('this is response ${response.data["name"][0]}');

        if (response.statusCode == 200) {
          //get image url from api response
          var recipies = response.data["name"];
          var percentages = response.data["percentage"];
          imageURL = response.data["name"][0];
          //print the image url
          print(imageURL);
          Get.snackbar('Success', "Image Uploaded Successfully",
              margin: EdgeInsets.only(top: 5, left: 10, right: 10));
          //loop throw recipies and 
        } else if (response.statusCode == 401) {
          Get.offAllNamed('/sign_up');
        } else {
          Get.snackbar('Failed', 'Error Code: ${response.statusCode}',
              margin: EdgeInsets.only(top: 5, left: 10, right: 10));
        }
      } else {
        Get.snackbar('Failed', 'Image not selected',
            margin: EdgeInsets.only(top: 5, left: 10, right: 10));
      }
    } finally {
      isLoading(false);
    }
  }
}
