import 'package:dio/dio.dart';

class ImageService{
  static Future<dynamic> uploadFile(filePath) async {
    try {
      FormData formData =
      new FormData.fromMap({
        "file":
        await MultipartFile.fromFile(filePath, filename: "file")});

      Response response =
      await Dio().post(
          "https://sea-of-food.herokuapp.com/predict",
          data: formData,
      );
      return response;
    }on DioError catch (e) {
      return e.response;
    } catch(e){
    }
  }
}